External packages used:

* next
* react
* fetch

To run it go to you repo folder on your console and type:

`npm run dev`

You will be able to access it through your browser on:

`http://localhost:3000/`
class FlickrAPI {   
    makeRequest(endPoint, params={}){ 
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();

            const url = `api/flickr/${endPoint}`;
            xhr.open("GET", url);

            xhr.onload = function(){
                if(xhr.readyState === 4 && xhr.status === 200){
                    resolve( JSON.parse(xhr.responseText));
                }else{
                    reject({
                        status: this.status,
                        statusText: xhr.statusText
                    })
                }
            }
            
            xhr.onerror = function () {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            };
            
            xhr.send(); 
        });
    }
   
    getLatestPics(){
      return this.makeRequest("latestPics");
    }  
  }

  export default FlickrAPI;
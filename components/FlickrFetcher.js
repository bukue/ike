import React from 'react';

import FlickrAPI from '../utils/FlickrAPI'

const buttonLabels ={
    fetch: 'Fetch Images',
    loading: 'Loading...'
}

class FlickrFetcher extends React.Component{
    constructor(props) {
        super(props);
      
        this.flickrAPI = new FlickrAPI();

        this.state = {
            pictures: [],
            buttonLabel: buttonLabels.fetch
        }
    }
    
    fetchImages = (event) => {
        this.setState({
            buttonLabel: buttonLabels.loading
        });

        this.flickrAPI.getLatestPics()
            .then( (response) => {
                this.setState({
                    pictures: response.items,
                    buttonLabel: buttonLabels.fetch
                });
            }).catch( (error) => {
                console.log(error);
            });
    }
    
    render() {
        return (
            <>
                <div>
                    <button onClick={this.fetchImages}>
                        {this.state.buttonLabel}
                    </button>
                </div>
                <div>
                    {
                        this.state.pictures.map((value, index) => {
                            return <img key={index} src={value.media.m} />
                        })
                    }
                </div>
            </>
        );
    }
}

export default FlickrFetcher;
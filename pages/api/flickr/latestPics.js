import fetch from 'node-fetch';

export default async (req, res) => {
    await fetch('https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1')
        .then( response => {
            return response.json();
        })
        .then(json => {
            return res.status(200).json({...json});
        })
        .catch(err => {
            console.log('err: ', err);
        })
};